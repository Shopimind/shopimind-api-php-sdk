<?php
namespace Shopimind\SdkShopimind;

class SpmOrdersCarriers
{
    use Traits\Methods;

    /**
     * Carrier identifier.
     * @var string
     */
    public $id_carrier;

    /**
     * Shop identifier if multiple shops are available.
     * @var string|null
     */
    public $id_shop;

    /**
     * Carrier name.
     * @var string
     */
    public $name;

    /**
     * Indicates if the carrier is active.
     * @var bool
     */
    public $is_active;

    /**
     * Creation date of the carrier in ISO 8601 format.
     * @var string
     */
    public $created_at;

    /**
     * Update date of the carrier in ISO 8601 format.
     * @var string
     */
    public $updated_at;

    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_carrier' => $this->id_carrier,
            'name' => $this->name,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }

        return $this->processSave( 'orders-carriers', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'orders-carriers', $data );
    }

    public function update(){
        $data = [
            'id_carrier' => $this->id_carrier,
            'id_shop' => $this->id_shop,
            'name' => $this->name,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'orders-carriers', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'orders-carriers', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'orders-carriers', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'orders-carriers/delete-batch', $data );
    }
}
