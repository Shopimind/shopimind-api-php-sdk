<?php
namespace Shopimind\SdkShopimind;

class SpmProductsVariations
{
    use Traits\Methods;

    /**
     * Product variation identifier.
     * @var string
     */
    public $id_variation;

    /**
     * Product identifier.
     * @var string
     */
    public $id_product;

    /**
     * Name of the product variation.
     * @var string
     */
    public $name;

    /**
     * Reference of the product variation, null if no reference.
     * @var string|null
     */
    public $reference;

    /**
     * EAN13 code of the product variation, null if no EAN13.
     * @var string|null
     */
    public $ean13;

    /**
     * Link to the product variation.
     * @var string
     */
    public $link;

    /**
     * Price of the product variation with 2 decimal places maximum.
     * @var float
     */
    public $price;

    /**
     * Discount price of the product variation with 2 decimal places maximum, null if no discount.
     * @var float|null
     */
    public $price_discount;

    /**
     * Quantity remaining of the product variation in stock.
     * @var int
     */
    public $quantity_remaining;

    /**
     * Indicates if the image is the default image of the product/variation.
     * @var bool
     */
    public $is_default;



    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_variation' => $this->id_variation,
            'id_product' => $this->id_product,
            'name' => $this->name,
            'reference' => $this->reference,
            'ean13' => $this->ean13,
            'link' => $this->link,
            'price' => $this->price,
            'price_discount' => $this->price_discount,
            'quantity_remaining' => $this->quantity_remaining,
            'is_default' => $this->is_default,
        ];

        return $this->processSave( 'products/variations', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'products/variations', $data );
    }

    public function update(){
        $data = [
            'id_variation' => $this->id_variation,
            'id_product' => $this->id_product,
            'name' => $this->name,
            'reference' => $this->reference,
            'ean13' => $this->ean13,
            'link' => $this->link,
            'price' => $this->price,
            'price_discount' => $this->price_discount,
            'quantity_remaining' => $this->quantity_remaining,
            'is_default' => $this->is_default,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'products/variations', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'products/variations', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'products/variations', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'products/variations/delete-batch', $data );
    }
}
