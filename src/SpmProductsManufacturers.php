<?php
namespace Shopimind\SdkShopimind;

class SpmProductsManufacturers
{
    use Traits\Methods;

    /**
     * Manufacturer identifier.
     * @var string
     */
    public $id_manufacturer;

    /**
     * Shop identifier if multiple shops are available.
     * @var string|null
     */
    public $id_shop;

    /**
     * Name of the manufacturer.
     * @var string
     */
    public $name;

    /**
     * Indicates if the manufacturer is active.
     * @var bool
     */
    public $is_active;

    /**
     * Creation date of the manufacturer in ISO 8601 format.
     * @var string
     */
    public $created_at;

    /**
     * Update date of the manufacturer in ISO 8601 format.
     * @var string
     */
    public $updated_at;


    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_manufacturer' => $this->id_manufacturer,
            'name' => $this->name,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }

        return $this->processSave( 'products-manufacturers', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'products-manufacturers', $data );
    }

    public function update(){
        $data = [
            'id_manufacturer' => $this->id_manufacturer,
            'id_shop' => $this->id_shop,
            'name' => $this->name,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'products-manufacturers', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'products-manufacturers', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'products-manufacturers', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'products-manufacturers/delete-batch', $data );
    }
}
