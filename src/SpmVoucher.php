<?php
namespace Shopimind\SdkShopimind;

class SpmVoucher
{
    use Traits\Methods;

    /**
     * Voucher identifier.
     * @var string
     */
    public $id_voucher;

    /**
     * Shop identifier if multiple shops are available.
     * @var string|null
     */
    public $id_shop;

    /**
     * Language associated with the voucher in ISO 639-1 format.
     * @var string
     */
    public $lang;

    /**
     * Voucher code.
     * @var string
     */
    public $code;

    /**
     * Description of the voucher.
     * @var string
     */
    public $description;

    /**
     * Discount voucher validity start date in ISO 8601 format.
     * @var string
     */
    public $started_at;

    /**
     * Discount voucher expiry date in ISO 8601 format, null if unlimited.
     * @var string|null
     */
    public $ended_at;

    /**
     * Customer identifier to whom the voucher is assigned. null if not assigned to any customer.
     * @var string|null
     */
    public $id_customer;

    /**
     * Type of the voucher.
     * @var string
     */
    public $type_voucher;

    /**
     * Discount voucher value, determined by amount or percentage, null if free shipping.
     * @var float|null
     */
    public $value;

    /**
     * Minimum amount required to use the voucher.
     * @var float
     */
    public $minimum_amount;

    /**
     * The currency code associated with the voucher in ISO 4217 format.
     * @var string
     */
    public $currency;

    /**
     * Indicates if the voucher is a tax reduction.
     * @var bool
     */
    public $reduction_tax;

    /**
     * Indicates if the voucher is used.
     * @var bool
     */
    public $is_used;

    /**
     * Indicates if the voucher is active.
     * @var bool
     */
    public $is_active;

    /**
     * Creation date of the voucher in ISO 8601 format.
     * @var string
     */
    public $created_at;

    /**
     * Update date of the voucher in ISO 8601 format.
     * @var string
     */
    public $updated_at;


    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_voucher' => $this->id_voucher,
            'lang' => $this->lang,
            'code' => $this->code,
            'description' => $this->description,
            'started_at' => $this->started_at,
            'ended_at' => $this->ended_at,
            'id_customer' => $this->id_customer,
            'type_voucher' => $this->type_voucher,
            'value' => $this->value,
            'minimum_amount' => $this->minimum_amount,
            'currency' => $this->currency,
            'reduction_tax' => $this->reduction_tax,
            'is_used' => $this->is_used,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }


        return $this->processSave( 'vouchers', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'vouchers', $data );
    }

    public function update(){
        $data = [
            'id_voucher' => $this->id_voucher,
            'id_shop' => $this->id_shop,
            'lang' => $this->lang,
            'code' => $this->code,
            'description' => $this->description,
            'started_at' => $this->started_at,
            'ended_at' => $this->ended_at,
            'id_customer' => $this->id_customer,
            'type_voucher' => $this->type_voucher,
            'value' => $this->value,
            'minimum_amount' => $this->minimum_amount,
            'currency' => $this->currency,
            'reduction_tax' => $this->reduction_tax,
            'is_used' => $this->is_used,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'vouchers', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'vouchers', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'vouchers', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'vouchers/delete-batch', $data );
    }
}
