<?php
namespace Shopimind\SdkShopimind;

class SpmProductsImages
{
    use Traits\Methods;

    /**
     * Image identifier.
     * @var string
     */
    public $id_image;

    /**
     * Product identifier.
     * @var string
     */
    public $id_product;

    /**
     * Product variation identifier associated with the image, null if no variation associated.
     * @var string|null
     */
    public $id_variation;

    /**
     * URL of the image.
     * @var string
     */
    public $url;

    /**
     * Indicates if the image is the default image of the product/variation.
     * @var bool
     */
    public $is_default;

    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_image' => $this->id_image,
            'id_product' => $this->id_product,
            'id_variation' => $this->id_variation,
            'url' => $this->url,
            'is_default' => $this->is_default,
        ];

        return $this->processSave( 'products/images', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'products/images', $data );
    }

    public function update(){
        $data = [
            'id_image' => $this->id_image,
            'id_product' => $this->id_product,
            'id_variation' => $this->id_variation,
            'url' => $this->url,
            'is_default' => $this->is_default,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'products/images', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'products/images', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'products/images', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'products/images/delete-batch', $data );
    }
}
