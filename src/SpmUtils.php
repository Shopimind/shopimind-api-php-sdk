<?php
namespace Shopimind\SdkShopimind;

use GuzzleHttp\Client as GuzzleClient;

class SpmUtils
{
    /**
     * @param $apiVersion
     * @param $apiKey
     * @return GuzzleClient
     */
    public static function getClient( $apiVersion, $apiKey ){
        $baseUrl = 'https://apidev2.shopimind.com';
        $baseUrl = rtrim($baseUrl, '/') . '/' . $apiVersion . '/';
        $httpClient = new GuzzleClient([
            'base_uri' => $baseUrl,
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'spm-api-key' => $apiKey,
            ],
        ]);
        return $httpClient;
    }
}
