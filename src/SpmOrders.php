<?php
namespace Shopimind\SdkShopimind;

class OrderProductsDTO {
    /**
     * Product identifier.
     * @var string
     */
    public $id_product;

    /**
     * Product variation identifier, null if is a simple product.
     * @var string|null
     */
    public $id_product_variation;

    /**
     * Price paid for the product with 2 decimal places maximum.
     * @var float
     */
    public $price;

    /**
     * Price paid for the product without tax with 2 decimal places maximum.
     * @var float
     */
    public $price_without_tax;

    /**
     * Manufacturer identifier of the product, null if no manufacturer.
     * @var string|null
     */
    public $id_manufacturer;

    /**
     * Quantity of the product in the order.
     * @var int
     */
    public $quantity;
}

class SpmOrders
{
    use Traits\Methods;

    /**
     * Order identifier.
     * @var string
     */
    public $id_order;

    /**
     * Shop identifier if multiple shops are available.
     * @var string|null
     */
    public $id_shop;

    /**
     * Language associated with the order in ISO 639-1 format.
     * @var string
     */
    public $lang;

    /**
     * Reference of the order, null if no reference.
     * @var string|null
     */
    public $reference;

    /**
     * Carrier identifier of the order, null if no carrier.
     * @var string|null
     */
    public $id_carrier;

    /**
     * Status identifier of the order.
     * @var string
     */
    public $id_status;

    /**
     * Address delivery identifier.
     * @var string
     */
    public $id_address_delivery;

    /**
     * Address invoice identifier.
     * @var string
     */
    public $id_address_invoice;

    /**
     * Customer email address associated with the order.
     * @var string
     */
    public $email_customer;

    /**
     * Array of order products.
     * @var OrderProductsDTO[]
     */
    public $products;

    /**
     * Cart identifier associated with the order.
     * @var string
     */
    public $id_cart;

    /**
     * Update date of the cart in ISO 8601 format.
     * @var string
     */
    public $cart_updated_at;

    /**
     * Total price of the order with 2 decimal places maximum.
     * @var float
     */
    public $amount;

    /**
     * Total price of the order without tax with 2 decimal places maximum.
     * @var float
     */
    public $amount_without_tax;

    /**
     * Shipping costs of the order with 2 decimal places maximum.
     * @var float
     */
    public $shipping_costs;

    /**
     * Shipping costs of the order without tax with 2 decimal places maximum.
     * @var float
     */
    public $shipping_costs_without_tax;

    /**
     * Shipping number associated with the order, null if no number.
     * @var string|null
     */
    public $shipping_number;

    /**
     * The currency code associated with the order in ISO 4217 format.
     * @var string
     */
    public $currency;

    /**
     * Voucher code used in the order, null if no voucher.
     * @var string|null
     */
    public $voucher_used;

    /**
     * Voucher value applied to the order (amount or percentage).
     * @var string|null
     */
    public $voucher_value;

    /**
     * Indicates if the order is confirmed (paid).
     * @var bool
     */
    public $is_confirmed;

    /**
     * Creation date of the order in ISO 8601 format.
     * @var string
     */
    public $created_at;

    /**
     * Update date of the order in ISO 8601 format.
     * @var string
     */
    public $updated_at;

    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_order' => $this->id_order,
            'lang' => $this->lang,
            'reference' => $this->reference,
            'id_carrier' => $this->id_carrier,
            'id_status' => $this->id_status,
            'id_address_delivery' => $this->id_address_delivery,
            'id_address_invoice' => $this->id_address_invoice,
            'email_customer' => $this->email_customer,
            'products' => $this->products,
            'id_cart' => $this->id_cart,
            'cart_updated_at' => $this->cart_updated_at,
            'amount' => $this->amount,
            'amount_without_tax' => $this->amount_without_tax,
            'shipping_costs' => $this->shipping_costs,
            'shipping_costs_without_tax' => $this->shipping_costs_without_tax,
            'shipping_number' => $this->shipping_number,
            'currency' => $this->currency,
            'voucher_used' => $this->voucher_used,
            'voucher_value' => $this->voucher_value,
            'is_confirmed' => $this->is_confirmed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];


        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }

        return $this->processSave( 'orders', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'orders', $data );
    }

    public function update(){
        $data = [
            'id_order' => $this->id_order,
            'id_shop' => $this->id_shop,
            'lang' => $this->lang,
            'reference' => $this->reference,
            'id_carrier' => $this->id_carrier,
            'id_status' => $this->id_status,
            'id_address_delivery' => $this->id_address_delivery,
            'id_address_invoice' => $this->id_address_invoice,
            'email_customer' => $this->email_customer,
            'products' => $this->products,
            'id_cart' => $this->id_cart,
            'cart_updated_at' => $this->cart_updated_at,
            'amount' => $this->amount,
            'amount_without_tax' => $this->amount_without_tax,
            'shipping_costs' => $this->shipping_costs,
            'shipping_costs_without_tax' => $this->shipping_costs_without_tax,
            'shipping_number' => $this->shipping_number,
            'currency' => $this->currency,
            'voucher_used' => $this->voucher_used,
            'voucher_value' => $this->voucher_value,
            'is_confirmed' => $this->is_confirmed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'orders', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'orders', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'orders', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'orders/delete-batch', $data );
    }
}
