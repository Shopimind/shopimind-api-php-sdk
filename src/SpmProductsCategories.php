<?php
namespace Shopimind\SdkShopimind;

class SpmProductsCategories
{
    use Traits\Methods;

    /**
     * Product category identifier.
     * @var string
     */
    public $id_category;

    /**
     * Shop identifier if multiple shops are available.
     * @var string|null
     */
    public $id_shop;

    /**
     * Language associated with the category in ISO 639-1 format.
     * @var string
     */
    public $lang;

    /**
     * Name of the category.
     * @var string
     */
    public $name;

    /**
     * Description of the category.
     * @var string
     */
    public $description;

    /**
     * Parent category identifier, null if no parent.
     * @var string|null
     */
    public $id_parent_category;

    /**
     * Link to the category.
     * @var string
     */
    public $link;

    /**
     * Indicates if the category is active.
     * @var bool
     */
    public $is_active;

    /**
     * Creation date of the category in ISO 8601 format.
     * @var string
     */
    public $created_at;

    /**
     * Update date of the category in ISO 8601 format.
     * @var string
     */
    public $updated_at;


    protected $auth;
    private $updateData = [];

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_category' => $this->id_category,
            'lang' => $this->lang,
            'name' => $this->name,
            'description' => $this->description,
            'id_parent_category' => $this->id_parent_category,
            'link' => $this->link,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }

        return $this->processSave( 'products-categories', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'products-categories', $data );
    }

    public function update(){
        $data = [
            'id_category' => $this->id_category,
            'id_shop' => $this->id_shop,
            'lang' => $this->lang,
            'name' => $this->name,
            'description' => $this->description,
            'id_parent_category' => $this->id_parent_category,
            'link' => $this->link,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'products-categories', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'products-categories', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'products-categories', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'products-categories/delete-batch', $data );
    }
}
