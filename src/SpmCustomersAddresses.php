<?php
namespace Shopimind\SdkShopimind;

class SpmCustomersAddresses
{
    use Traits\Methods;

    /**
     * Customer address identifier
     * @var string
     */
    public $id_address;

    /**
     * Customer identifier
     * @var string
     */
    public $id_customer;

    /**
     * First name associated with the address
     * @var string
     */
    public $first_name;

    /**
     * Last name associated with the address
     * @var string
     */
    public $last_name;

    /**
     * Primary phone number associated with the address, null if not provided
     * @var string|null
     */
    public $primary_phone;

    /**
     * Secondary phone number associated with the address, null if not provided
     * @var string|null
     */
    public $secondary_phone;

    /**
     * Company name associated with the address, null if not provided
     * @var string|null
     */
    public $company;

    /**
     * Address line 1
     * @var string
     */
    public $address_line_1;

    /**
     * Address line 2, null if not provided
     * @var string|null
     */
    public $address_line_2;

    /**
     * The postal code of the address
     * @var string
     */
    public $postal_code;

    /**
     * City of the address
     * @var string
     */
    public $city;

    /**
     * Country of the address
     * @var string
     */
    public $country;

    /**
     * Indicates if the address is active
     * @var boolean
     */
    public $is_active;

    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_address' => $this->id_address,
            'id_customer' => $this->id_customer,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'primary_phone' => $this->primary_phone,
            'secondary_phone' => $this->secondary_phone,
            'company' => $this->company,
            'address_line_1' => $this->address_line_1,
            'address_line_2' => $this->address_line_2,
            'postal_code' => $this->postal_code,
            'city' => $this->city,
            'country' => $this->country,
            'is_active' => $this->is_active
        ];

        return $this->processSave( 'customers/addresses', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'customers/addresses', $data );
    }

    public function update()
    {
        $data = [
            'id_address' => $this->id_address,
            'id_customer' => $this->id_customer,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'primary_phone' => $this->primary_phone,
            'secondary_phone' => $this->secondary_phone,
            'company' => $this->company,
            'address_line_1' => $this->address_line_1,
            'address_line_2' => $this->address_line_2,
            'postal_code' => $this->postal_code,
            'city' => $this->city,
            'country' => $this->country,
            'is_active' => $this->is_active
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'customers/addresses', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'customers/addresses', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'customers/addresses', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'customers/addresses/delete-batch', $data );
    }

}
