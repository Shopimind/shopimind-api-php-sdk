<?php
namespace Shopimind\SdkShopimind;

class SpmProducts
{
    use Traits\Methods;

    /**
     * Product identifier.
     * @var string
     */
    public $id_product;

    /**
     * Shop identifier if multiple shops are available.
     * @var string|null
     */
    public $id_shop;

    /**
     * Language associated with the product in ISO 639-1 format.
     * @var string
     */
    public $lang;

    /**
     * Name of the product.
     * @var string
     */
    public $name;

    /**
     * Reference of the product, null if no reference.
     * @var string|null
     */
    public $reference;

    /**
     * EAN13 code of the product, null if no EAN13.
     * @var string|null
     */
    public $ean13;

    /**
     * Description of the product.
     * @var string
     */
    public $description;

    /**
     * Short description of the product, null if no short description.
     * @var string|null
     */
    public $description_short;

    /**
     * Link to the product.
     * @var string
     */
    public $link;

    /**
     * Array of category identifiers of the product.
     * @var string[]|null
     */
    public $ids_categories;

    /**
     * Manufacturer identifier of the product, null if no manufacturer.
     * @var string|null
     */
    public $id_manufacturer;

    /**
     * The currency code of the product in ISO 4217 format.
     * @var string
     */
    public $currency;

    /**
     * Price of the product with 2 decimal places maximum.
     * @var float
     */
    public $price;

    /**
     * Discount price of the product with 2 decimal places maximum, null if no discount.
     * @var float|null
     */
    public $price_discount;

    /**
     * Quantity remaining of the product in stock.
     * @var int
     */
    public $quantity_remaining;

    /**
     * Indicates if the product is active.
     * @var bool
     */
    public $is_active;

    /**
     * Creation date of the product in ISO 8601 format.
     * @var string
     */
    public $created_at;

    /**
     * Update date of the product in ISO 8601 format.
     * @var string
     */
    public $updated_at;


    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_product' => $this->id_product,
            'lang' => $this->lang,
            'name' => $this->name,
            'reference' => $this->reference,
            'ean13' => $this->ean13,
            'description' => $this->description,
            'description_short' => $this->description_short,
            'link' => $this->link,
            'ids_categories' => $this->ids_categories,
            'id_manufacturer' => $this->id_manufacturer,
            'currency' => $this->currency,
            'price' => $this->price,
            'price_discount' => $this->price_discount,
            'quantity_remaining' => $this->quantity_remaining,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }

        return $this->processSave( 'products', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'products', $data );
    }

    public function update(){
        $data = [
            'id_product' => $this->id_product,
            'id_shop' => $this->id_shop,
            'lang' => $this->lang,
            'name' => $this->name,
            'reference' => $this->reference,
            'ean13' => $this->ean13,
            'description' => $this->description,
            'description_short' => $this->description_short,
            'link' => $this->link,
            'ids_categories' => $this->ids_categories,
            'id_manufacturer' => $this->id_manufacturer,
            'currency' => $this->currency,
            'price' => $this->price,
            'price_discount' => $this->price_discount,
            'quantity_remaining' => $this->quantity_remaining,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'products', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'products', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'products', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'products/delete-batch', $data );
    }
}
