<?php
namespace Shopimind\SdkShopimind;
use GuzzleHttp\Client as GuzzleClient;

class SpmOrdersStatus
{
    use Traits\Methods;
    

    /**
     * Order status identifier
     * @var string
     */
    public $id_status;

    /**
     * Shop identifier if multiple shops are available. (optional)
     * @var string|null
     */
    public $id_shop;

    /**
     * Language associated with the order status in ISO 639-1 format
     * @var string
     */
    public $lang;

    /**
     * Order status name
     * @var string
     */
    public $name;

    /**
     * Indicates if the order status is deleted
     * @var bool
     */
    public $is_deleted;

    /**
     * Creation date of the order status in ISO 8601 format
     * @var string
     */
    public $created_at;

    /**
     * Update date of the order status in ISO 8601 format
     * @var string
     */
    public $updated_at;

    /**
     * Client for authentication
     * @var GuzzleClient
     */
    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_status' => $this->id_status,
            'lang' => $this->lang,
            'name' => $this->name,
            'is_deleted' => $this->is_deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }

        return $this->processSave( 'orders-status', $data );
    }

    /**
     * @param $auth GuzzleClient
     * @return mixed
     */
    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'orders-status', $data );
    }

    public function update(){
        $data = [
            'id_status' => $this->id_status,
            'lang' => $this->lang,
            'name' => $this->name,
            'is_deleted' => $this->is_deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'orders-status', $updateData);
    }

    /**
     * @param $auth GuzzleClient
     * @return mixed
     */
    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'orders-status', $data );
    }

    /**
     * @param $auth GuzzleClient
     * @param $id string
     * @return mixed
     */
    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'orders-status', $id );
    }

    /**
     * @param $auth GuzzleClient
     * @param string[] $data
     * @return mixed
     */
    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'orders-status/delete-batch', $data );
    }
}
