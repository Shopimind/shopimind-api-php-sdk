<?php
namespace Shopimind\SdkShopimind;

class SpmCustomersGroups
{
    use Traits\Methods;

    /**
     * Customer group identifier
     * @var string
     */
    public $id_group;

    /**
     * Shop identifier if multiple shops are available, null if not provided
     * @var string|null
     */
    public $id_shop;

    /**
     * Language associated with the customer group in ISO 639-1 format
     * @var string
     */
    public $lang;

    /**
     * Customer group name
     * @var string
     */
    public $name;

    /**
     * Creation date of the customer group in ISO 8601 format
     * @var string
     */
    public $created_at;

    /**
     * Update date of the customer group in ISO 8601 format
     * @var string
     */
    public $updated_at;


    protected $auth;

    public function __construct($auth) {
        $this->auth = $auth;
    }

    public function save()
    {
        $data = [
            'id_group' => $this->id_group,
            'lang' => $this->lang,
            'name' => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];

        if ($this->id_shop) {
            $data['id_shop'] = $this->id_shop;
        }

        return $this->processSave( 'customers-groups', $data );
    }

    public static function saveBatch( $auth, $data )
    {
        return self::processSaveBatch( $auth, 'customers-groups', $data );
    }

    public function update(){
        $data = [
            'id_group' => $this->id_group,
            'id_shop' => $this->id_shop,
            'lang' => $this->lang,
            'name' => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];

        $updateData = [];
        foreach ($data as $key => $value) {
            if ( !empty( $value ) ) {
                $updateData[$key] = $value;
            }
        }

        return $this->processUpdate( 'customers-groups', $updateData );
    }

    public static function updateBatch( $auth, $data )
    {
        return self::processUpdateBatch( $auth, 'customers-groups', $data );
    }

    public static function delete( $auth, $id )
    {
        return self::processDelete( $auth, 'customers-groups', $id );
    }

    public static function deleteBatch( $auth, $data )
    {
        return self::processDeleteBatch( $auth, 'customers-groups/delete-batch', $data );
    }
}
